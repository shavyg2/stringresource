var fs = require("fs");
var path = require("path");
var os = require("os");
var root = (os.platform() == "win32") ? process.cwd().split(path.sep)[0] + "\\" : "/";
var fsbm;
var Promise = require("bluebird");
var async = require("async");
fsbm = require("form-submit");
var flatten = require("flat");
var Merge = fsbm.Merge;

var mkdirp = require("mkdirp");
var PackageJs = function(searchdir) {
    var location = searchdir || process.cwd();
    location = location.replace(/(\/|\\)$/, "");
    location = path.resolve(location);
    while (location != root) {
        var result = fs.statSync(location);

        try {
            var filepath = path.resolve(location, "package.json");
            var file = fs.statSync(filepath);
            return location;
        } catch (e) {

        }
        location = path.resolve(location, "..");
    }
    throw new Error("Can't find Package.json file");
}

function StringResource(dir) {
    this.initilize(dir);
}



var sr = StringResource.prototype;

sr.initilize = function(dir) {
    var location = PackageJs(dir);
    var Start = path.resolve(location, "Resources");
    this.start = Start;
}

sr.setDirectory = function(dir) {
    this.initilize(dir);
}


sr.parse = function(key, value) {
    key = key.toLowerCase();
    var split = key.split(".");
    var wait = false;
    var folder = path.resolve(this.start, split.shift());
    var store = path.resolve(folder, "data.json");

    var data = {};
    var _data = data;
    var prev;
    var k;
    while (split.length) {
        k = split.shift();
        _data[k] = {};
        prev = _data;
        _data = _data[k];
    }
    prev[k] = value;
    return {
        folder: folder,
        store: store,
        data: data,
        value: prev[k]
    }
}


sr.getLegacy = function(key, cb) {
    key = key.toLowerCase();
    var split = key.split(".");
    var file = path.resolve(this.start, split.shift(), "data.json");
    fs.readFile(file, {
        encoding: "utf8"
    }, function(err, content) {
        //console.log(arguments);
        if (!err) {
            content = JSON.parse(content);
            var value = content;

            try {
                while (split.length) {
                    var k = split.shift();
                    value = value[k];
                }
                cb(value);
            } catch (e) {
                cb(null);
            }
        } else {
            cb(null);
        }
    });
}


function ReadObjectFromFolder(base) {
    return new Promise(function(resolve, reject) {
        var structure = {};
        fs.readdir(base, function(err, files) {
            if (err) {
                reject(err);
                return;
            }
            var code = [];
            var index;
            if (index = files.indexOf("data.json") >= 0) {
                files.splice(index, 1);
            }

            for (var key in files) {
                code.push((function(file) {

                    return function(next) {
                        if (file.split(".").length === 1) {
                            ReadObjectFromFolder(path.resolve(base, file))
                                .then(function(content) {
                                    debugger;
                                    structure[file] = content;
                                    next();
                                })
                        } else {
                            fs.readFile(path.resolve(base, "data.txt"), {
                                "encoding": "utf8"
                            }, function(err, content) {
                                if (!err) {
                                    if (files.length === 1)
                                        resolve(content);
                                    next()
                                } else {
                                    reject(err);
                                    next();
                                }
                            })
                        }

                    }
                })(files[key]))
            }

            async.parallel(code, function() {
                resolve(structure);
            })
        })
    });
}


function ReadObjectFromFolderSync(base) {
    var structure = {};
    var code = [];
    var index;
    var files = fs.readdirSync(base);

    if (index = files.indexOf("data.json") >= 0) {
        files.splice(index, 1);
    }

    for (var key in files) {
        var file = files[key];
        if (file.split(".").length === 1) {
            structure[file] = ReadObjectFromFolderSync(path.resolve(base, file));
        } else {
            var content = fs.readFileSync(path.resolve(base, "data.txt"), {
                "encoding": "utf8"
            });
            if (files.length === 1) {
                return content;
            }
        }
    }
    return structure;
}



sr.getSyncLegacy = function(key) {
    key = key.toLowerCase();
    var split = key.split(".");
    var file = path.resolve(this.start, split.shift(), "data.json");
    try {
        var content = fs.readFileSync(file, {
            encoding: "utf8"
        });
        content = JSON.parse(content);
        var value = content;
        while (split.length) {
            var k = split.shift();
            value = value[k];
        }
        return value;
    } catch (e) {
        return null;
    }
}


sr.get = function(key, cb) {
    var self = this;
    return new Promise(function(resolve, reject) {
        var folder = path.resolve(self.start, key.replace(/[.]/g, "/"));
        ReadObjectFromFolder(folder).then(function(obj) {
            resolve(obj);
        }).catch(function(err) {
            resolve();
        });
    }).then(function(content) {
        return new Promise(function(resolve, reject) {
            self.getLegacy(key, function(content2) {
                if (typeof cb === "function") {
                    if (content) {
                        if (typeof content === "object")
                            content = Merge(content, content2 || {})
                        cb(content);
                    } else {
                        cb(content2);
                    }
                } else {
                    if (content) {
                        if (typeof content === "object")
                            content = Merge(content, content2 || {})
                        resolve(content);
                    } else {
                        resolve(content2);
                    }
                }
            })
        })
    }).catch(function() {
        cb(null);
    })
}

sr.getSync = function(key) {
    var self = this;

    var folder = path.resolve(self.start, key.replace(/[.]/g, "/"));
    try {
        var content = ReadObjectFromFolderSync(folder);
    } catch (e) {
        content=null;
    }
    var content2 = self.getSyncLegacy(key);
    if (content) {
        content = Merge(content, content2 || {});
    } else {
        content = content2;
    }
    return content;
}


function StringToObject(str, value) {
    var obj = {};
    str = str.split(".");

    if (str.length === 1) {
        obj[str.shift()] = value;
        return obj;
    } else {
        var key = str.shift();
        obj[key] = StringToObject(str.join("."), value);
        return obj;
    }
}

function ObjectSave(base, obj) {
    return new Promise(function(resolve, reject) {
        obj = flatten(obj);
        for (var key in obj) {
            var folder = key.replace(/[.]/g, "/");
            var content = obj[key];
            folder = path.resolve(base, folder);
            mkdirp(folder, function(err) {
                if (!err) {
                    var file = path.resolve(folder, "data.txt");
                    fs.writeFile(file, content, function(err) {
                        if (err) {
                            reject(err);
                        } else {
                            resolve(file);
                        }
                    })
                } else {
                    reject(err);
                }
            })
        }
    })
}

function ObjectSaveSync(base, obj) {
    for (var k in obj) {
        var item = obj[k];
        var folder = path.resolve(base, k);
        var file = path.resolve(folder, "data.txt");
        try {
            fs.mkdirSync(folder);
        } catch (e) {}
        switch (typeof item) {
            case "object":
                ObjectSaveSync(folder, item);
                break;
                break;
            case "string":
                fs.writeFileSync(file, item, {
                    flags: "w+"
                });
                break;
        }
    }
}

sr.set = function(key, value, cb) {

    var o = this.parse(key, value);
    var folder = o.folder;
    var store = o.store;
    var data = o.data;

    fs.stat(folder, function(err, stat) {

        var next = function() {

            var next = function() {
                fs.readFile(store, {
                    encoding: "utf8"
                }, function(err, content) {
                    if (!err) {
                        content = JSON.parse(content);
                        var merge = fsbm.Merge(data, content);
                        fs.writeFile(store, JSON.stringify(merge), {
                            encoding: "utf8"
                        }, cb);
                    } else {
                        throw err;
                    }
                });
            }
            fs.stat(store, function(err, stat) {
                if (!err) {
                    next();
                } else {
                    fs.writeFile(store, "{}", null, next);
                }
            })
        }

        if (!err) {
            //console.log("Found it");
            next();
        } else {
            mkdirp(folder, function(err) {
                if (!err) {
                    next();
                } else {
                    throw err;
                }
            });
        }
    });
}


sr.set = function(key, value, cb) {
    key = key.toLowerCase();
    var obj = StringToObject(key, value);
    ObjectSave(this.start, obj)
        .then(function() {
            cb();
        })
        .catch(function(err) {
            console.log(err);
            cb(err);
        });
}


sr.setSync = function(key, value, cb) {
    key = key.toLowerCase();
    var obj = StringToObject(key, value);
    ObjectSaveSync(this.start, obj);
}

sr.delete = function(key, cb) {
    key = key.toLowerCase();
    var o = this.parse(key);
    var folder = o.folder;
    var file = path.resolve(folder, "data.json");

    fs.readFile(file, {
        encoding: "utf8"
    }, function(err, content) {
        if (!err) {
            content = JSON.parse(content);
            var split = key.split(".");
            split.shift();


            var data = content;
            var _data = data;
            var prev;
            var k;
            while (split.length) {
                k = split.shift();
                prev = _data;
                _data = _data[k];
            }
            delete prev[k];
            var str = JSON.stringify(data);
            fs.writeFile(file, str, {
                encoding: "utf8"
            }, function(err) {
                if (!err && cb instanceof Function)
                    cb(err, true);
                else if (cb instanceof Function)
                    cb(err, false);
            })

        } else {
            if (cb instanceof Function)
                cb(err, false);
        }
    })
}


module.exports = new StringResource();

//console.log(StringToObject("user.name.first.letter.language"));

/*
var resource = new StringResource();

var str="en-US.Application.CatchPhrase";
resource.set(str,"We're right around the corners",function(){
    str=resource.getSync(str);
    console.log(str);
});

*/