#!/usr/bin/env node

var r = require("./index");
var arg = process.argv.slice(2);
var action = arg[0];
var key = arg[1];
var value = arg[2];

switch(action.toLowerCase()){
    case "set":
        r.set(key,value,function(){
            console.log("Success");
        });
        break;
    case "get":
        r.get(key,function(result){
            switch(typeof result){
                case "object":
                console.log(JSON.stringify(result,null,2));
                    break;
                default:
                    console.log(result);
            }
        })
        break;
    case "delete":
        r.delete(key);
        break;
}