var resource = require("../index");
var fs = require("fs");
var path = require("path");


var chai = require("chai");
var should = chai.should();
var expect = chai.expect;


describe("A resource using the set method", function() {
	var file = path.resolve(__dirname, "../Resources/person/name/data.txt");
	before(function(done) {
		try {
			fs.unlinkSync(file);
		} catch (e) {
			console.log(e);
		}
		resource.set("person.name", "shavauhn", done);
	})

	it("should create file", function(done) {
		fs.readFile(file, function(err, buffer) {
			expect(err).to.equal(null);
			buffer.toString().should.equal("shavauhn");
			done();
		})
	})

})


describe("A resource using the setSync method", function() {
	var file = path.resolve(__dirname, "../Resources/person/name/data.txt");
	before(function() {
		try {
			fs.unlinkSync(file);
		} catch (e) {
			console.log(e);
		}
		resource.setSync("person.name", "shavauhn");
	})

	it("should create file", function(done) {
		fs.readFile(file, function(err, buffer) {
			expect(err).to.equal(null);
			buffer.toString().should.equal("shavauhn");
			done();
		})
	})

})



//GET
describe("A resource using the get promise method", function() {
	var file = path.resolve(__dirname, "../Resources/person/name/data.txt");
	var name;
	before(function(done) {
		resource.get("person.name").then(function(n) {
			name = n;
			done();
		});
	})

	it("should read the resource", function() {
		expect(name).to.equal("shavauhn");
	})
})


describe("A resource using the get callback method", function() {
	var file = path.resolve(__dirname, "../Resources/person/name/data.txt");
	var name;
	before(function(done) {
		resource.get("person.name", function(n) {
			name = n;
			done();
		});
	})

	it("should read the resource", function() {
		expect(name).to.equal("shavauhn");
	})

})

describe("A resource using the get promise method legacy", function() {
	var file = path.resolve(__dirname, "../Resources/person/name/data.txt");
	var name;
	before(function(done) {
		resource.get("application.name").then(function(n) {
			name = n;
			done();
		});
	})

	it("should create file", function() {
		expect(name).to.equal("Banana");
	})
})


describe("A resource using the getLegacy method", function() {
	var file = path.resolve(__dirname, "../Resources/person/name/data.txt");
	var name;
	before(function(done) {
		resource.get("application.name", function(n) {
			name = n;
			done();
		});
	})

	it("should create file", function() {
		expect(name).to.equal("Banana");
	})

})